﻿// Copyright (c) 2016 Mitsuhiro Ito (nee)
//
// This software is released under the MIT License.
// http://opensource.org/licenses/mit-license.php

using NeeView.ComponentModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace NeeView
{
    /// <summary>
    /// 本の操作
    /// </summary>
    public class BookOperation : BindableBase
    {
        // System Object
        public static BookOperation Current;

        #region events

        // ブックが変更された
        public event EventHandler BookChanged;

        // ページ表示内容の更新
        //public event EventHandler<ViewSource> ViewContentsChanged;

        // ページが変更された
        public event EventHandler<PageChangedEventArgs> PageChanged;

        // ページがソートされた
        public event EventHandler PagesSorted;

        // ページが削除された
        // TODO: EventArgs
        public event EventHandler<Page> PageRemoved;


        #endregion


        // ページ終端でのアクション
        public PageEndAction PageEndAction { get; set; }


        //
        public BookOperation(BookHub bookHub)
        {
            Current = this;

            _bookHub = bookHub;

            _bookHub.BookChanged +=
                (s, e) => SetBook(_bookHub.BookUnit);

            _bookHub.Loading +=
                (s, e) =>
                {
                    if (_bookHub.IsLoading) IsEnabled = false;
                };
        }

        //
        private BookHub _bookHub;

        /// <summary>
        /// 本の更新
        /// </summary>
        /// <param name="bookUnit"></param>
        public void SetBook(BookUnit bookUnit)
        {
            this.BookUnit = bookUnit;

            if (this.BookUnit != null)
            {
                this.Book.PageChanged += Book_PageChanged;
                this.Book.PagesSorted += Book_PagesSorted;
                this.Book.PageTerminated += Book_PageTerminated;
                this.Book.PageRemoved += Book_PageRemoved;
            }

            //
            RaisePropertyChanged(nameof(IsBookmark));

            // マーカー復元
            // TODO: PageMarkersのしごと？
            UpdatePagemark();

            // ページリスト更新
            UpdatePageList();

            // ブック操作有効
            IsEnabled = true;

            BookChanged?.Invoke(this, null);
        }

        //
        private void Book_PagesSorted(object sender, EventArgs e)
        {
            UpdatePageList();
            PagesSorted?.Invoke(this, e);
        }

        //
        private void Book_PageChanged(object sender, PageChangedEventArgs e)
        {
            RaisePropertyChanged(nameof(IsPagemark));
            PageChanged?.Invoke(this, e);
        }


        /// <summary>
        /// IsEnabled property.
        /// ロード中は機能を無効にするため
        /// </summary>
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { if (_isEnabled != value) { _isEnabled = value; RaisePropertyChanged(); } }
        }

        private bool _isEnabled;



        /// <summary>
        /// Book property.
        /// </summary>
        public BookUnit BookUnit
        {
            get { return _bookUnit; }
            set { if (_bookUnit != value) { _bookUnit = value; RaisePropertyChanged(); } }
        }

        private BookUnit _bookUnit;

        //
        public Book Book => _bookUnit?.Book;

        //
        public string Place => _bookUnit?.Book.Place;

        //
        public bool IsValid => _bookUnit != null;


        /// <summary>
        /// PaageList
        /// </summary>
        public ObservableCollection<Page> PageList
        {
            get { return _pageList; }
            set
            {
                _pageList = value;
                RaisePropertyChanged();
            }
        }

        private ObservableCollection<Page> _pageList;


        // ページリスト更新
        // TODO: クリアしてもサムネイルのListBoxは項目をキャッシュしてしまうので、なんとかせよ
        // サムネイル用はそれに特化したパーツのみ提供する？
        // いや、ListBoxを独立させ、それ自体を作り直す方向で？んー？
        // 問い合わせがいいな。
        // 問い合わせといえば、BitmapImageでOutOfMemoryが取得できない問題も。
        public void UpdatePageList()
        {
            var pages = this.Book?.Pages;
            PageList = pages != null ? new ObservableCollection<Page>(pages) : null;

            RaisePropertyChanged(nameof(IsPagemark));
        }


        // 現在ページ番号取得
        public int GetPageIndex()
        {
            return this.Book == null ? 0 : this.Book.DisplayIndex; // GetPosition().Index;
        }

        // 現在ページ番号を設定し、表示を切り替える (先読み無し)
        public void RequestPageIndex(object sender, int index)
        {
            this.Book?.RequestSetPosition(sender, new PagePosition(index, 0), 1, false);
        }

        /// <summary>
        /// 最大ページ番号取得
        /// </summary>
        /// <returns></returns>
        public int GetMaxPageIndex()
        {
            var count = this.Book == null ? 0 : this.Book.Pages.Count - 1;
            if (count < 0) count = 0;
            return count;
        }

        /// <summary>
        /// ページ数取得
        /// </summary>
        /// <returns></returns>
        public int GetPageCount()
        {
            return this.Book == null ? 0 : this.Book.Pages.Count;
        }


        /// <summary>
        /// ファイルロック解除
        /// </summary>
        public void Unlock()
        {
            this.Book?.Unlock();
        }



        /// <summary>
        /// 外部アプリ設定
        /// </summary>
        public ExternalApplication ExternalApplication
        {
            get { return _ExternalApplication; }
            set { if (_ExternalApplication != value) { _ExternalApplication = value ?? new ExternalApplication(); RaisePropertyChanged(); } }
        }

        private ExternalApplication _ExternalApplication = new ExternalApplication();


        /// <summary>
        /// クリップボード
        /// </summary>
        public ClipboardUtility ClipboardUtility
        {
            get { return _ClipboardUtility; }
            set { if (_ClipboardUtility != value) { _ClipboardUtility = value ?? new ClipboardUtility(); RaisePropertyChanged(); } }
        }

        private ClipboardUtility _ClipboardUtility = new ClipboardUtility();




        #region BookCommand : ページ削除

        // 現在表示しているページのファイル削除可能？
        public bool CanDeleteFile()
        {
            return FileIOProfile.Current.IsEnabled && FileIO.Current.CanRemoveFile(Book?.GetViewPage());
        }

        // 現在表示しているページのファイルを削除する
        public async void DeleteFile()
        {
            if (CanDeleteFile())
            {
                await FileIO.Current.RemoveFile(Book?.GetViewPage());
            }
        }

        #endregion

        #region BookCommand : ページ出力

        // ファイルの場所を開くことが可能？
        public bool CanOpenFilePlace()
        {
            return Book?.GetViewPage() != null;
        }

        // ファイルの場所を開く
        public void OpenFilePlace()
        {
            if (CanOpenFilePlace())
            {
                string place = Book.GetViewPage()?.GetFilePlace();
                if (place != null)
                {
                    System.Diagnostics.Process.Start("explorer.exe", "/select,\"" + place + "\"");
                }
            }
        }




        // 外部アプリで開く
        public void OpenApplication()
        {
            if (CanOpenFilePlace())
            {
                try
                {
                    this.ExternalApplication.Call(Book?.GetViewPages());
                }
                catch (Exception e)
                {
                    new MessageDialog($"原因: {e.Message}", "外部アプリ実行に失敗しました").ShowDialog();
                }
            }
        }


        // クリップボードにコピー
        public void CopyToClipboard()
        {
            if (CanOpenFilePlace())
            {
                try
                {
                    this.ClipboardUtility.Copy(Book?.GetViewPages());
                }
                catch (Exception e)
                {
                    new MessageDialog($"原因: {e.Message}", "コピーに失敗しました").ShowDialog();
                }
            }
        }


        /// <summary>
        /// ファイル保存可否
        /// </summary>
        /// <returns></returns>
        public bool CanExport()
        {
            var pages = Book?.GetViewPages();
            if (pages == null || pages.Count == 0) return false;

            var bitmapSource = (pages[0].Content as BitmapContent)?.BitmapSource;
            if (bitmapSource == null) return false;

            return true;
        }

        // ファイルに保存する
        // TODO: OutOfMemory対策
        public void Export()
        {
            if (CanExport())
            {
                try
                {
                    var pages = Book.GetViewPages();
                    int index = Book.GetViewPageindex() + 1;
                    string name = $"{Path.GetFileNameWithoutExtension(Book.Place)}_{index:000}-{index + pages.Count - 1:000}.png";
                    var exporter = new Exporter();
                    exporter.Initialize(pages, Book.BookReadOrder, name);
                    exporter.Background = ContentCanvasBrush.Current.CreateBackgroundBrush();
                    exporter.BackgroundFront = ContentCanvasBrush.Current.CreateBackgroundFrontBrush(new DpiScale(1, 1));
                    if (exporter.ShowDialog() == true)
                    {
                        try
                        {
                            exporter.Export();
                        }
                        catch (Exception e)
                        {
                            new MessageDialog($"原因: {e.Message}", "ファイル保存に失敗しました").ShowDialog();
                        }
                    }
                }
                catch (Exception e)
                {
                    new MessageDialog($"この画像は出力できません。\n原因: {e.Message}", "ファイル保存に失敗しました").ShowDialog();
                    return;
                }
            }
        }


        #endregion

        #region BookCommand : ページ操作

        // ページ終端を超えて移動しようとするときの処理
        private void Book_PageTerminated(object sender, int e)
        {
            // TODO ここでSlideShowを参照しているが、引数で渡すべきでは？
            if (SlideShow.Current.IsPlayingSlideShow && SlideShow.Current.IsSlideShowByLoop)
            {
                FirstPage();
            }

            else if (this.PageEndAction == PageEndAction.Loop)
            {
                if (e < 0)
                {
                    LastPage();
                }
                else
                {
                    FirstPage();
                }
            }
            else if (this.PageEndAction == PageEndAction.NextFolder)
            {
                if (e < 0)
                {
                    FolderList.Current.PrevFolder(BookLoadOption.LastPage);
                }
                else
                {
                    FolderList.Current.NextFolder(BookLoadOption.FirstPage);
                }
            }
            else
            {
                if (SlideShow.Current.IsPlayingSlideShow)
                {
                    // スライドショー解除
                    SlideShow.Current.IsPlayingSlideShow = false;
                }

                else if (e < 0)
                {
                    InfoMessage.Current.SetMessage(InfoMessageType.Notify, "最初のページです");
                }
                else
                {
                    InfoMessage.Current.SetMessage(InfoMessageType.Notify, "最後のページです");
                }
            }
        }


        // ページ削除時の処理
        private void Book_PageRemoved(object sender, Page e)
        {
            // ページマーカーから削除
            RemovePagemark(new Pagemark(this.Book.Place, e.FullPath));

            UpdatePageList();
            PageRemoved?.Invoke(sender, e);
        }


        // 前のページに移動
        public void PrevPage()
        {
            this.Book?.PrevPage();
        }

        // 次のページに移動
        public void NextPage()
        {
            this.Book?.NextPage();
        }

        // 1ページ前に移動
        public void PrevOnePage()
        {
            this.Book?.PrevPage(1);
        }

        // 1ページ後に移動
        public void NextOnePage()
        {
            this.Book?.NextPage(1);
        }

        // 指定ページ数前に移動
        public void PrevSizePage(int size)
        {
            this.Book?.PrevPage(size);
        }

        // 指定ページ数後に移動
        public void NextSizePage(int size)
        {
            this.Book?.NextPage(size);
        }


        // 最初のページに移動
        public void FirstPage()
        {
            this.Book?.FirstPage();
        }

        // 最後のページに移動
        public void LastPage()
        {
            this.Book?.LastPage();
        }

        // 指定ページに移動
        public void JumpPage(Page page)
        {
            if (_isEnabled && page != null) this.Book?.JumpPage(page);
        }

        // スライドショー用：次のページへ移動
        public void NextSlide()
        {
            if (SlideShow.Current.IsPlayingSlideShow) NextPage();
        }

        #endregion

        #region BookCommand : ブックマーク

        // ブックマーク登録可能？
        public bool CanBookmark()
        {
            return (Book != null);
        }

        // ブックマーク切り替え
        public void ToggleBookmark()
        {
            if (CanBookmark())
            {
                if (BookUnit.Book.Place.StartsWith(Temporary.TempDirectory))
                {
                    new MessageDialog($"原因: 一時フォルダーはブックマークできません", "ブックマークできません").ShowDialog();
                }
                else
                {
                    BookUnit.BookMementoUnit = BookmarkCollection.Current.Toggle(BookUnit.BookMementoUnit, Book.CreateMemento());
                    RaisePropertyChanged(nameof(IsBookmark));
                }
            }
        }

        // ブックマーク判定
        public bool IsBookmark
        {
            get
            {
                if (BookUnit?.BookMementoUnit != null && BookUnit.BookMementoUnit.Memento.Place == Book.Place)
                {
                    return BookUnit.BookMementoUnit.BookmarkNode != null;
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion


        #region BookCommand : ページマーク

        // ページマークにに追加、削除された
        public event EventHandler<PagemarkChangedEventArgs> PagemarkChanged;

        //
        public bool IsPagemark
        {
            get { return IsMarked(); }
        }

        // 表示ページのマーク判定
        public bool IsMarked()
        {
            return this.Book != null ? this.Book.IsMarked(this.Book.GetViewPage()) : false;
        }

        // ページマーク登録可能？
        public bool CanPagemark()
        {
            return (this.Book != null);
        }

        // マーカー切り替え
        public void TogglePagemark()
        {
            if (!_isEnabled || this.Book == null) return;

            if (Current.Book.Place.StartsWith(Temporary.TempDirectory))
            {
                new MessageDialog($"原因: 一時フォルダーはページマークできません", "ページマークできません").ShowDialog();
            }

            // マーク登録/解除
            // TODO: 登録時にサムネイルキャッシュにも登録
            PagemarkCollection.Current.Toggle(new Pagemark(this.Book.Place, this.Book.GetViewPage().FullPath));

            // 更新
            UpdatePagemark();
        }


        // マーカー削除
        public void RemovePagemark(Pagemark mark)
        {
            PagemarkCollection.Current.Remove(mark);
            UpdatePagemark(mark);
        }

        /// <summary>
        /// マーカー表示更新
        /// </summary>
        /// <param name="mark">変更や削除されたマーカー</param>
        public void UpdatePagemark(Pagemark mark)
        {
            // 現在ブックに影響のある場合のみ更新
            if (this.Book?.Place == mark.Place)
            {
                UpdatePagemark();
            }
        }

        // マーカー表示更新
        public void UpdatePagemark()
        {
            // 本にマーカを設定
            // TODO: これはPagemarkerの仕事？
            this.Book?.SetMarkers(PagemarkCollection.Current.Collect(this.Book.Place).Select(e => e.EntryName));

            // 表示更新
            PagemarkChanged?.Invoke(this, null);
            RaisePropertyChanged(nameof(IsPagemark));
        }

        //
        public bool CanPrevPagemarkInPlace(MovePagemarkCommandParameter param)
        {
            return (this.Book?.Markers != null && Current.Book.Markers.Count > 0) || param.IsIncludeTerminal;
        }

        //
        public bool CanNextPagemarkInPlace(MovePagemarkCommandParameter param)
        {
            return (this.Book?.Markers != null && Current.Book.Markers.Count > 0) || param.IsIncludeTerminal;
        }

        // ページマークに移動
        public void PrevPagemarkInPlace(MovePagemarkCommandParameter param)
        {
            if (!_isEnabled || this.Book == null) return;
            var result = this.Book.RequestJumpToMarker(this, -1, param.IsLoop, param.IsIncludeTerminal);
            if (!result)
            {
                InfoMessage.Current.SetMessage(InfoMessageType.Notify, "現在ページより前のページマークはありません");
            }
        }

        public void NextPagemarkInPlace(MovePagemarkCommandParameter param)
        {
            if (!_isEnabled || this.Book == null) return;
            var result = this.Book.RequestJumpToMarker(this, +1, param.IsLoop, param.IsIncludeTerminal);
            if (!result)
            {
                InfoMessage.Current.SetMessage(InfoMessageType.Notify, "現在ページより後のページマークはありません");
            }
        }

        // ページマークに移動
        public bool JumpPagemarkInPlace(Pagemark mark)
        {
            if (mark == null) return false;

            if (mark.Place == this.Book?.Place)
            {
                Page page = this.Book.GetPage(mark.EntryName);
                if (page != null)
                {
                    JumpPage(page);
                    return true;
                }
            }

            return false;
        }

        #endregion


        #region Memento
        [DataContract]
        public class Memento
        {
            [DataMember]
            public PageEndAction PageEndAction { get; set; }
            [DataMember]
            public ExternalApplication ExternalApplication { get; set; }
            [DataMember]
            public ClipboardUtility ClipboardUtility { get; set; }
        }

        //
        public Memento CreateMemento()
        {
            var memento = new Memento();
            memento.PageEndAction = this.PageEndAction;
            memento.ExternalApplication = ExternalApplication.Clone();
            memento.ClipboardUtility = ClipboardUtility.Clone();
            return memento;
        }

        //
        public void Restore(Memento memento)
        {
            if (memento == null) return;
            this.PageEndAction = memento.PageEndAction;
            this.ExternalApplication = memento.ExternalApplication?.Clone();
            this.ClipboardUtility = memento.ClipboardUtility?.Clone();
        }
        #endregion

    }
}
