﻿// Copyright (c) 2016 Mitsuhiro Ito (nee)
//
// This software is released under the MIT License.
// http://opensource.org/licenses/mit-license.php

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeeView
{
    // BookMementoCollectionChangedイベントの種類
    public enum BookMementoCollectionChangedType
    {
        Load,
        Clear,
        Add,
        Update,
        Remove,
    }

    // BookMementoCollectionChangedイベントの引数
    public class BookMementoCollectionChangedArgs : EventArgs
    {
        public BookMementoCollectionChangedType HistoryChangedType { get; set; }
        public string Key { get; set; }

        public BookMementoCollectionChangedArgs(BookMementoCollectionChangedType type, string key)
        {
            HistoryChangedType = type;
            Key = key;
        }
    }


    /// <summary>
    /// BookMementoUnit用ノード
    /// </summary>
    public class BookMementoUnitNode : IHasPage
    {
        public BookMementoUnit Value { get; set; }

        public BookMementoUnitNode(BookMementoUnit value)
        {
            Value = value;
        }

        public Page GetPage()
        {
            return Value?.ArchivePage;
        }
    }

    /// <summary>
    /// 高速検索用BookMemento辞書
    /// 履歴、ブックマーク共有の辞書です
    /// SQL使いたくなってきた..
    /// </summary>
    public class BookMementoUnit : IHasPage
    {
        // 履歴用リンク
        public LinkedListNode<BookMementoUnit> HistoryNode { get; set; }

        // ブックマーク用リンク
        public BookMementoUnitNode BookmarkNode { get; set; }

        // ページマーク用リンク
        public BookMementoUnitNode PagemarkNode { get; set; }

        // 本体
        public Book.Memento Memento { get; set; }

        //
        public override string ToString()
        {
            return Memento?.Place ?? base.ToString();
        }

        /// <summary>
        /// ArchivePage Property.
        /// サムネイル用。保存しません
        /// </summary>
        private ArchivePage _archivePage;
        public ArchivePage ArchivePage
        {
            get
            {
                if (_archivePage == null && Memento != null)
                {
                    _archivePage = new ArchivePage(Memento.Place);
                    _archivePage.Thumbnail.IsSupprtedCache = true;
                    _archivePage.Thumbnail.Touched += Thumbnail_Touched;
                }
                return _archivePage;
            }
            set { _archivePage = value; }
        }

        //
        private void Thumbnail_Touched(object sender, EventArgs e)
        {
            var thumbnail = (Thumbnail)sender;
            PanelThumbnailPool.Current.Add(thumbnail);
        }

        //
        public Page GetPage()
        {
            return ArchivePage;
        }
    }


    /// <summary>
    /// 履歴
    /// </summary>
    public class BookMementoCollection
    {
        public static BookMementoCollection Current { get; private set; }

        //
        public BookMementoCollection()
        {
            Current = this;
        }

        public Dictionary<string, BookMementoUnit> Items { get; set; } = new Dictionary<string, BookMementoUnit>();

        private BookMementoUnit _lastFindUnit;

        //
        public void Add(BookMementoUnit unit)
        {
            Debug.Assert(unit != null);
            Debug.Assert(unit.Memento != null);
            Debug.Assert(unit.Memento.Place != null);
            Debug.Assert(unit.HistoryNode != null || unit.BookmarkNode != null || unit.PagemarkNode != null);

            Items.Add(unit.Memento.Place, unit);
        }

        //
        public BookMementoUnit Find(string place)
        {
            if (place == null) return null;

            // 最後に検索されたユニットは再度検索される時に高速にする
            if (place == _lastFindUnit?.Memento.Place) return _lastFindUnit;

            BookMementoUnit unit;
            Items.TryGetValue(place, out unit);
            _lastFindUnit = unit;
            return unit;
        }

        //
        internal void Rename(string src, string dst)
        {
            if (src == null || dst == null) return;

            var unit = Find(src);
            if (unit != null)
            {
                this.Items.Remove(src);

                unit.Memento.Place = dst;
                this.Items.Add(dst, unit);
            }
        }
    }
}
