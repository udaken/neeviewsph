﻿<!--
    Copyright (c) 2016 Mitsuhiro Ito (nee)

    This software is released under the MIT License.
    http://opensource.org/licenses/mit-license.php
-->
    
<UserControl x:Class="NeeView.HistoryListBox"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
             xmlns:local="clr-namespace:NeeView"
             mc:Ignorable="d" 
             d:DesignHeight="300" d:DesignWidth="300"
             Background="{DynamicResource NVBackground}"
             Foreground="{DynamicResource NVForeground}">

    <UserControl.Resources>

        <local:BookMementoToTooltipConverter x:Key="BookMementoToTooltipConverter"/>

        <!-- Default TextBlock -->
        <Style TargetType="TextBlock">
            <Setter Property="FontSize" Value="15"/>
        </Style>

        <Style TargetType="TextBlock" x:Key="FolderName">
            <Setter Property="Text" Value="{Binding Memento.Name}"/>
            <Setter Property="ToolTip" Value="{Binding Memento, Converter={StaticResource BookMementoToTooltipConverter}}"/>
            <Setter Property="ToolTipService.InitialShowDelay" Value="1000"/>
            <Setter Property="ToolTipService.BetweenShowDelay" Value="1000"/>
            <Setter Property="Margin" Value="5,0"/>
            <Setter Property="HorizontalAlignment" Value="Left"/>
            <Setter Property="VerticalAlignment" Value="Center"/>
            <Setter Property="FontSize" Value="15"/>
            <Setter Property="TextTrimming" Value="CharacterEllipsis"/>
        </Style>

        <Style TargetType="TextBlock" x:Key="FolderParameter">
            <Setter Property="Margin" Value="5,0"/>
            <Setter Property="VerticalAlignment" Value="Center"/>
            <Setter Property="FontSize" Value="15"/>
            <Setter Property="Foreground" Value="Gray"/>
            <Setter Property="TextTrimming" Value="CharacterEllipsis"/>
        </Style>

        <local:FileSizeToStringConverter x:Key="FileSizeToStringConverter"/>

        <!-- Normal History -->
        <DataTemplate x:Key="NormalHistory">
            <TextBlock Style="{StaticResource FolderName}"/>
        </DataTemplate>

        <!-- Picture History -->
        <DataTemplate x:Key="PictureHistory">
            <Border Margin="0,1" BorderBrush="#22888888" BorderThickness="0,0,0,1">
                <DockPanel Margin="2">
                    <DockPanel>
                        <local:FolderListThumbnail DataContext="{Binding ArchivePage.Thumbnail}"/>
                        <StackPanel VerticalAlignment="Center">
                            <TextBlock x:Name="Place" Style="{StaticResource FolderParameter}" Text="{Binding ArchivePage.Entry.RootArchiverName}" />
                            <TextBlock x:Name="FileNameTextBlock" Style="{StaticResource FolderName}" TextWrapping="NoWrap"/>
                        </StackPanel>
                    </DockPanel>
                </DockPanel>
            </Border>
        </DataTemplate>

        <!-- Banner History -->
        <DataTemplate x:Key="BannerHistory">
            <Border Margin="0,1" BorderBrush="#22888888" BorderThickness="0,0,0,1">
                <DockPanel Margin="0" HorizontalAlignment="Stretch" LastChildFill="False">
                    <DockPanel DockPanel.Dock="Bottom" Margin="2" >
                        <TextBlock x:Name="FileNameTextBlock" Style="{StaticResource FolderName}" Margin="0" />
                    </DockPanel>
                    <local:FolderListBanner DataContext="{Binding ArchivePage.Thumbnail}" d:DataContext="{d:DesignInstance local:Thumbnail_Design, IsDesignTimeCreatable=True}"/>
                </DockPanel>
            </Border>
        </DataTemplate>

    </UserControl.Resources>
    <ListBox x:Name="ListBox" Focusable="False"
                 Background="Transparent"
                 Foreground="{Binding Foreground, RelativeSource={RelativeSource AncestorType=UserControl}}"
                 FocusVisualStyle="{StaticResource NVFocusVisual}"
                 ScrollViewer.HorizontalScrollBarVisibility="Disabled"
                 HorizontalContentAlignment="Stretch"
                 ItemsSource="{Binding Items}"
                 SelectedItem="{Binding SelectedItem}"
                 KeyDown="HistoryListBox_KeyDown"
                 SelectionChanged="HistoryListBox_SelectionChanged" 
                 IsVisibleChanged="HistoryListBox_IsVisibleChanged"
                 VirtualizingPanel.IsVirtualizing="True"
                 VirtualizingPanel.ScrollUnit="{StaticResource PanelScrollUnit}"
                 VirtualizingStackPanel.VirtualizationMode="Recycling"
                 >

        <ListBox.Style>
            <Style TargetType="ListBox" BasedOn="{StaticResource NVListBox}">
                <Style.Triggers>
                    <DataTrigger Binding="{Binding Model.PanelListItemStyle}" Value="Normal">
                        <Setter Property="ItemTemplate" Value="{StaticResource NormalHistory}"/>
                    </DataTrigger>
                    <DataTrigger Binding="{Binding Model.PanelListItemStyle}" Value="Content">
                        <Setter Property="ItemTemplate" Value="{StaticResource PictureHistory}"/>
                    </DataTrigger>
                    <DataTrigger Binding="{Binding Model.PanelListItemStyle}" Value="Banner">
                        <Setter Property="ItemTemplate" Value="{StaticResource BannerHistory}"/>
                    </DataTrigger>
                </Style.Triggers>
            </Style>
        </ListBox.Style>

        <ListBox.Resources>
            <Style TargetType="ListBoxItem">
                <EventSetter Event="PreviewMouseLeftButtonUp" Handler="HistoryListItem_MouseSingleClick"/>
                <EventSetter Event="KeyDown" Handler="HistoryListItem_KeyDown"/>
                <Setter Property="FocusVisualStyle" Value="{StaticResource NVFocusVisual}"/>

                <Setter Property="ContextMenu">
                    <Setter.Value>
                        <ContextMenu>
                            <MenuItem Header="履歴から削除(D)" Command="{x:Static local:HistoryListBox.RemoveCommand}"/>
                        </ContextMenu>
                    </Setter.Value>
                </Setter>
            </Style>

        </ListBox.Resources>

    </ListBox>

</UserControl>
