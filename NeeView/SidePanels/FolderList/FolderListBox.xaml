﻿<!--
    Copyright (c) 2016 Mitsuhiro Ito (nee)

    This software is released under the MIT License.
    http://opensource.org/licenses/mit-license.php
-->
<UserControl x:Class="NeeView.FolderListBox"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
             xmlns:local="clr-namespace:NeeView"
             mc:Ignorable="d" 
             d:DataContext="{d:DesignInstance local:FolderListDataContext_Design, IsDesignTimeCreatable=True}"
             d:DesignHeight="300" d:DesignWidth="300"
             Background="{DynamicResource NVBackground}"
             Foreground="{DynamicResource NVForeground}">

    <UserControl.Resources>

        <Style TargetType="TextBlock">
            <Setter Property="FontSize" Value="15"/>
        </Style>

        <Style x:Key="BlackStyle" TargetType="UserControl">
            <Setter Property="Background" Value="#FF101010"/>
            <Setter Property="Foreground" Value="White"/>
        </Style>
        <Style x:Key="WhiteStyle" TargetType="UserControl">
            <Setter Property="Background" Value="#FFF8F8F8"/>
            <Setter Property="Foreground" Value="Black"/>
        </Style>

        <local:FullPathToFileNameConverter x:Key="FullPathToFileNameConverter"/>

        <Style x:Key="HistoryMenuItemContainerStyle" TargetType="MenuItem">
            <Setter Property="Header" Value="{Binding Path=Value, Converter={StaticResource FullPathToFileNameConverter}}"/>
            <Setter Property="Command" Value="{Binding DataContext.MoveToHistory, RelativeSource={RelativeSource FindAncestor, AncestorType=ContextMenu}}" />
            <Setter Property="CommandParameter" Value="{Binding}" />
        </Style>


        <BooleanToVisibilityConverter x:Key="BooleanToVisibilityConverter"/>
        <local:BooleanReverseConverter x:Key="BooleanReverseConverter"/>
        <local:NullableToVisibilityConverter x:Key="NullableToVisibilityConverter"/>

        <local:FolderIconLayoutToVisibilityConverter x:Key="FolderIconLayoutToVisibilityConverter"/>

        <Style TargetType="TextBlock" x:Key="FolderName">
            <Setter Property="Text" Value="{Binding Name}"/>
            <Setter Property="ToolTipService.IsEnabled" Value="{Binding ElementName=ListBox, Path=DataContext.IsRenaming, Converter={StaticResource BooleanReverseConverter}}"/>
            <Setter Property="ToolTip" Value="{Binding Name}"/>
            <Setter Property="ToolTipService.InitialShowDelay" Value="1000"/>
            <Setter Property="ToolTipService.BetweenShowDelay" Value="1000"/>
            <Setter Property="Margin" Value="5,0"/>
            <Setter Property="VerticalAlignment" Value="Center"/>
            <Setter Property="FontSize" Value="15"/>
            <Setter Property="TextTrimming" Value="CharacterEllipsis"/>
        </Style>

        <Style TargetType="TextBlock" x:Key="FolderParameter">
            <Setter Property="Margin" Value="5,0"/>
            <Setter Property="VerticalAlignment" Value="Center"/>
            <Setter Property="FontSize" Value="15"/>
            <Setter Property="Foreground" Value="Gray"/>
            <Setter Property="TextTrimming" Value="CharacterEllipsis"/>
        </Style>

        <local:FileSizeToStringConverter x:Key="FileSizeToStringConverter"/>

        <!-- Normal -->
        <DataTemplate x:Key="NormalFolder">
            <DockPanel LastChildFill="False">
                <local:FolderListIcon DockPanel.Dock="Right"/>
                <TextBlock x:Name="FileNameTextBlock" Style="{StaticResource FolderName}"/>
            </DockPanel>
        </DataTemplate>

        <!-- Normal 2-->
        <DataTemplate x:Key="NormalFolder2">
            <DockPanel LastChildFill="False">
                <local:FolderListIconMark DockPanel.Dock="Right"/>
                <local:FolderIcon DockPanel.Dock="Left" />
                <TextBlock x:Name="FileNameTextBlock" Style="{StaticResource FolderName}"/>
            </DockPanel>
        </DataTemplate>

        <!-- Content -->
        <DataTemplate x:Key="PictureFolder">
            <Border Margin="0,1" BorderBrush="#22888888" BorderThickness="0,0,0,1">
                <DockPanel LastChildFill="False" Margin="2">
                    <local:FolderListIcon DockPanel.Dock="Right" />
                    <local:FolderListThumbnail DataContext="{Binding ArchivePage.Thumbnail}" d:DataContext="{d:DesignInstance local:Thumbnail_Design, IsDesignTimeCreatable=True}"/>
                    <StackPanel VerticalAlignment="Center">
                        <TextBlock x:Name="FileNameTextBlock" Style="{StaticResource FolderName}" TextWrapping="NoWrap" HorizontalAlignment="Left"/>
                        <TextBlock x:Name="LastUpdateTime" Style="{StaticResource FolderParameter}" Text="{Binding ArchivePage.LastWriteTime, StringFormat={}{0:yyyy/MM/dd HH:mm:ss}}" />
                        <TextBlock x:Name="FileSize" Style="{StaticResource FolderParameter}" Text="{Binding ArchivePage.Length, Converter={StaticResource FileSizeToStringConverter}}" />
                    </StackPanel>
                </DockPanel>
            </Border>
        </DataTemplate>

        <!-- Content 2 -->
        <DataTemplate x:Key="PictureFolder2">
            <Border Margin="0,1" BorderBrush="#22888888" BorderThickness="0,0,0,1">
                <DockPanel LastChildFill="False" Margin="2">
                    <local:FolderListIconMark DockPanel.Dock="Right" />
                    <local:FolderListThumbnail DataContext="{Binding ArchivePage.Thumbnail}" d:DataContext="{d:DesignInstance local:Thumbnail_Design, IsDesignTimeCreatable=True}"/>
                    <StackPanel VerticalAlignment="Center">
                        <DockPanel>
                            <local:FolderIcon DockPanel.Dock="Left" />
                            <TextBlock x:Name="FileNameTextBlock" Style="{StaticResource FolderName}" TextWrapping="NoWrap" HorizontalAlignment="Left"/>
                        </DockPanel>
                        <TextBlock x:Name="LastUpdateTime" Style="{StaticResource FolderParameter}" Text="{Binding ArchivePage.LastWriteTime, StringFormat={}{0:yyyy/MM/dd HH:mm:ss}}" />
                        <TextBlock x:Name="FileSize" Style="{StaticResource FolderParameter}" Text="{Binding ArchivePage.Length, Converter={StaticResource FileSizeToStringConverter}}" />
                    </StackPanel>
                </DockPanel>
            </Border>
        </DataTemplate>

        <!-- Banner -->
        <DataTemplate x:Key="BannerFolder">
            <Border Margin="0,1" BorderBrush="#22888888" BorderThickness="0,0,0,1">
                <DockPanel Margin="0" HorizontalAlignment="Stretch" LastChildFill="False">
                    <DockPanel DockPanel.Dock="Bottom" Margin="2" >
                        <local:FolderListIcon DockPanel.Dock="Right"/>
                        <TextBlock x:Name="FileNameTextBlock" Style="{StaticResource FolderName}" Margin="0" />
                    </DockPanel>
                    <local:FolderListBanner DataContext="{Binding ArchivePage.Thumbnail}" d:DataContext="{d:DesignInstance local:Thumbnail_Design, IsDesignTimeCreatable=True}"/>
                </DockPanel>
            </Border>
        </DataTemplate>

        <!-- BannerFolder 2-->
        <DataTemplate x:Key="BannerFolder2">
            <Border Margin="0,1" BorderBrush="#22888888" BorderThickness="0,0,0,1">
                <DockPanel Margin="0" HorizontalAlignment="Stretch" LastChildFill="False">
                    <DockPanel DockPanel.Dock="Bottom" Margin="2" >
                        <local:FolderListIconMark DockPanel.Dock="Right"/>
                        <local:FolderIcon DockPanel.Dock="Left" />
                        <TextBlock x:Name="FileNameTextBlock" Style="{StaticResource FolderName}" />
                    </DockPanel>
                    <local:FolderListBanner DataContext="{Binding ArchivePage.Thumbnail}" d:DataContext="{d:DesignInstance local:Thumbnail_Design, IsDesignTimeCreatable=True}"/>
                </DockPanel>
            </Border>
        </DataTemplate>


    </UserControl.Resources>


    <ListBox x:Name="ListBox" Focusable="True" 
                Background="Transparent"
                Foreground="{Binding Foreground, RelativeSource={RelativeSource AncestorType=UserControl}}"
                FocusVisualStyle="{StaticResource NVFocusVisual}"
                ScrollViewer.HorizontalScrollBarVisibility="Disabled"
                ItemsSource="{Binding Model.FolderCollection.Items}"
                SelectedItem="{Binding Model.SelectedItem}"
                SelectionChanged="FolderList_SelectionChanged"
                Loaded="FolderList_Loaded"
                PreviewKeyDown="FolderList_PreviewKeyDown"
                KeyDown="FolderList_KeyDown"
                UseLayoutRounding="True"
                VirtualizingStackPanel.IsVirtualizing="True"
                VirtualizingStackPanel.ScrollUnit="{StaticResource PanelScrollUnit}"
                VirtualizingStackPanel.VirtualizationMode="Recycling"
             >

        <ListBox.Style>
            <Style TargetType="ListBox" BasedOn="{StaticResource NVListBox}">
                <Style.Triggers>
                    <MultiDataTrigger>
                        <MultiDataTrigger.Conditions>
                            <Condition Binding="{Binding Model.PanelListItemStyle}" Value="Normal"/>
                            <Condition Binding="{Binding Model.FolderIconLayout}" Value="Default"/>
                        </MultiDataTrigger.Conditions>
                        <Setter Property="ItemTemplate" Value="{StaticResource NormalFolder}"/>
                    </MultiDataTrigger>
                    <MultiDataTrigger>
                        <MultiDataTrigger.Conditions>
                            <Condition Binding="{Binding Model.PanelListItemStyle}" Value="Normal"/>
                            <Condition Binding="{Binding Model.FolderIconLayout}" Value="Explorer"/>
                        </MultiDataTrigger.Conditions>
                        <Setter Property="ItemTemplate" Value="{StaticResource NormalFolder2}"/>
                    </MultiDataTrigger>

                    <MultiDataTrigger>
                        <MultiDataTrigger.Conditions>
                            <Condition Binding="{Binding Model.PanelListItemStyle}" Value="Content"/>
                            <Condition Binding="{Binding Model.FolderIconLayout}" Value="Default"/>
                        </MultiDataTrigger.Conditions>
                        <Setter Property="ItemTemplate" Value="{StaticResource PictureFolder}"/>
                    </MultiDataTrigger>
                    <MultiDataTrigger>
                        <MultiDataTrigger.Conditions>
                            <Condition Binding="{Binding Model.PanelListItemStyle}" Value="Content"/>
                            <Condition Binding="{Binding Model.FolderIconLayout}" Value="Explorer"/>
                        </MultiDataTrigger.Conditions>
                        <Setter Property="ItemTemplate" Value="{StaticResource PictureFolder2}"/>
                    </MultiDataTrigger>

                    <MultiDataTrigger>
                        <MultiDataTrigger.Conditions>
                            <Condition Binding="{Binding Model.PanelListItemStyle}" Value="Banner"/>
                            <Condition Binding="{Binding Model.FolderIconLayout}" Value="Default"/>
                        </MultiDataTrigger.Conditions>
                        <Setter Property="ItemTemplate" Value="{StaticResource BannerFolder}"/>
                    </MultiDataTrigger>
                    <MultiDataTrigger>
                        <MultiDataTrigger.Conditions>
                            <Condition Binding="{Binding Model.PanelListItemStyle}" Value="Banner"/>
                            <Condition Binding="{Binding Model.FolderIconLayout}" Value="Explorer"/>
                        </MultiDataTrigger.Conditions>
                        <Setter Property="ItemTemplate" Value="{StaticResource BannerFolder2}"/>
                    </MultiDataTrigger>
                </Style.Triggers>
            </Style>
        </ListBox.Style>

        <ListBox.ItemContainerStyle>
            <Style TargetType="{x:Type ListBoxItem}">
                <EventSetter Event="PreviewMouseLeftButtonUp" Handler="FolderListItem_MouseSingleClick"/>
                <EventSetter Event="PreviewMouseDoubleClick" Handler="FolderListItem_MouseDoubleClick"/>
                <EventSetter Event="KeyDown" Handler="FolderListItem_KeyDown"/>
                <EventSetter Event="PreviewMouseDown" Handler="FolderListItem_MouseDown"/>
                <EventSetter Event="PreviewMouseUp" Handler="FolderListItem_MouseUp"/>
                <EventSetter Event="PreviewMouseMove" Handler="FolderListItem_MouseMove"/>
                <Setter Property="FocusVisualStyle" Value="{StaticResource NVFocusVisual}"/>
                <Setter Property="HorizontalAlignment" Value="Stretch"/>
                <Setter Property="VerticalAlignment" Value="Center"/>
                <Setter Property="HorizontalContentAlignment" Value="Stretch"/>
                <Setter Property="VerticalContentAlignment" Value="Center"/>
                <Setter Property="ContextMenu">
                    <Setter.Value>
                        <ContextMenu>
                            <MenuItem Header="サブフォルダーを読み込む" Command="{x:Static local:FolderListBox.LoadWithRecursiveCommand}"/>
                            <Separator/>
                            <MenuItem Header="エクスプローラーで開く" Command="{x:Static local:FolderListBox.OpenExplorerCommand}"/>
                            <MenuItem Header="コピー(C)" Command="{x:Static local:FolderListBox.CopyCommand}"/>
                            <Separator/>
                            <MenuItem Header="削除(D)" Command="{x:Static local:FolderListBox.RemoveCommand}"/>
                            <MenuItem Header="名前の変更(M)" Command="{x:Static local:FolderListBox.RenameCommand}"/>
                        </ContextMenu>
                    </Setter.Value>
                </Setter>
            </Style>
        </ListBox.ItemContainerStyle>

    </ListBox>

</UserControl>
