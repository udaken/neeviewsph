﻿// Copyright (c) 2016 Mitsuhiro Ito (nee)
//
// This software is released under the MIT License.
// http://opensource.org/licenses/mit-license.php

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Susie
{
    public class SpiException : ApplicationException
    {
        public SpiException(string msg, SusiePlugin spi) : base($"[{System.IO.Path.GetFileName(spi.FileName)}] {msg}")
        {
        }
    }


    /// <summary>
    /// Susie アーカイブエントリ
    /// </summary>
    public class ArchiveEntry
    {
        // 対応するプラグイン
        private SusiePlugin _spi;

        // エントリ情報(RAW)
        private ArchiveFileInfoRaw _info;

        // アーカイブのショートファイル名
        // UNICODE対応のため、ショートファイル名でアクセスします
        public string ArchiveShortFileName { get; private set; }

        // エントリのパス名
        public string Path => _info.path;

        // エントリ名
        public string FileName => _info.filename;

        // 展開後ファイルサイズ
        public long FileSize => (long)_info.filesize;

        // タイムスタンプ
        public DateTime TimeStamp => Time_T2DateTime((uint)_info.timestamp);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ArchiveEntry(SusiePlugin spi, string archiveFileName, ArchiveFileInfoRaw info)
        {
            _spi = spi;
            ArchiveShortFileName = archiveFileName;
            _info = info;
        }


        /// メモリに展開
        public byte[] Load()
        {
            lock (_spi.Lock)
            {
                using (var api = _spi.Open())
                {
                    var buff = api.GetFile(ArchiveShortFileName, _info);
                    if (buff == null) throw new SpiException("抽出に失敗しました(M)", _spi);
                    return buff;
                }
            }
        }

        /// フォルダーに出力。ファイル名は変更しない
        public void ExtractToFolder(string extractFolder)
        {
            lock (_spi.Lock)
            {
                using (var api = _spi.Open())
                {
                    int ret = api.GetFile(ArchiveShortFileName, _info, extractFolder);
                    if (ret != 0) throw new SpiException($"抽出に失敗しました(F)", _spi);
                }
            }
        }

        /// ファイルに出力
        public void ExtractToFile(string extract)
        {
            using (var ms = new System.IO.MemoryStream(Load(), false))
            using (var stream = new System.IO.FileStream(extract, System.IO.FileMode.Create))
            {
                ms.WriteTo(stream);
            }
        }

        // UNIX時間をDateTimeに変換
        private static DateTime Time_T2DateTime(uint time_t)
        {
            long win32FileTime = 10000000 * (long)time_t + 116444736000000000;
            return DateTime.FromFileTime(win32FileTime);
        }
    }


    /// <summary>
    /// Susie アーカイブエントリ リスト
    /// </summary>
    public class ArchiveEntryCollection : List<ArchiveEntry>
    {
        public SusiePlugin SusiePlugin { get; set; }

        public ArchiveEntryCollection(SusiePlugin spi, string archiveFileName, List<ArchiveFileInfoRaw> entries)
        {
            SusiePlugin = spi;

            string shortPath = Win32Api.GetShortPathName(archiveFileName);
            foreach (var entry in entries)
            {
                this.Add(new ArchiveEntry(spi, shortPath, entry));
            }
        }
    }
}
