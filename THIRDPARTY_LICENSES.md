## Third-Party Licenses

### 7-Zip

DLL: 7z.dll

* Copyright (C) 1999-2016 Igor Pavlov.
* URL: [7-Zip](http://www.7-zip.org/)
* License: [GNU LGPL + unRAR restriction](http://www.7-zip.org/license.txt)

### SevenZipSharp

DLL: SevenZipSharpNE.dll

* Copyright (C) Markovtsev Vadim 2009, 2010
* URL: [SevenZipSharp - Home](https://sevenzipsharp.codeplex.com/)
* License: [GNU LGPL](https://sevenzipsharp.codeplex.com/license)
* Changed:
    * VS 2015 project: [Project Page](https://github.com/tomap/SevenZipSharp)
    * RAR5 Support: [Project Page](https://github.com/neelabo/SevenZipSharp)

### Material design icons

ICON: ic_help_24px, etc...

* Google (C)
* URL: [Icons - Style - Google design guidelines](http://www.google.com/design/spec/style/icons.html#icons-system-icons)
* License: [CC-BY 4.0](http://creativecommons.org/licenses/by/4.0/)

### WPFDragAndDropSample

Code-in:

* Copyright (c) 2016 takanemu
* URL: [WPFDragAndDropSample](https://github.com/takanemu/WPFDragAndDropSample)
* License: [MIT License](https://github.com/takanemu/WPFDragAndDropSample/blob/master/LICENSE)

### PDFium

DLL: pdfium.dll

* Copyright 2014 PDFium Authors. All rights reserved.
* URL: [PDFium](https://pdfium.googlesource.com/pdfium/)
* License: [PDFium LICENSE](https://pdfium.googlesource.com/pdfium/+/master/LICENSE)

### PdfiumViewer

DLL: PdfiumViewer.dll

* Copyright (c) Pieter van Ginkel
* URL: [PdfiumViewer](https://github.com/pvginkel/PdfiumViewer)
* License: [Apache License 2.0](https://github.com/pvginkel/PdfiumViewer/blob/master/LICENSE)
